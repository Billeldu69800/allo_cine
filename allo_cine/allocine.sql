-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 11 mars 2020 à 15:33
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `allocine`
--

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `id_client` int(11) NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(50) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `mot_de_passe` varchar(50) NOT NULL,
  PRIMARY KEY (`id_client`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `film`
--

DROP TABLE IF EXISTS `film`;
CREATE TABLE IF NOT EXISTS `film` (
  `id_film` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(30) NOT NULL,
  `date_sortie` date NOT NULL,
  `resume` text NOT NULL,
  `cover` varchar(30) DEFAULT NULL,
  `fk_realisateur` int(11) NOT NULL,
  `fk_genre` int(11) NOT NULL,
  PRIMARY KEY (`id_film`),
  KEY `genre` (`fk_genre`),
  KEY `personne` (`fk_realisateur`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `film`
--

INSERT INTO `film` (`id_film`, `titre`, `date_sortie`, `resume`, `cover`, `fk_realisateur`, `fk_genre`) VALUES
(7, 'a', '2020-03-11', 'zzzzddd', NULL, 1, 1),
(14, 'azertya8', '2020-03-11', 'zzzzddd', NULL, 2, 3),
(21, 'aa', '2020-03-11', 'a', 'null', 1, 1),
(22, 'Les Enfants de la soie', '2020-03-11', 'Attaquant les enfants dans les prairie', 'null', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `film_acteur`
--

DROP TABLE IF EXISTS `film_acteur`;
CREATE TABLE IF NOT EXISTS `film_acteur` (
  `fk_film` int(11) NOT NULL,
  `fk_acteur` int(11) NOT NULL,
  KEY `fk_film` (`fk_film`),
  KEY `fk_acteur` (`fk_acteur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `film_format`
--

DROP TABLE IF EXISTS `film_format`;
CREATE TABLE IF NOT EXISTS `film_format` (
  `id_format_film` int(11) NOT NULL AUTO_INCREMENT,
  `fk_format` int(11) NOT NULL,
  `fk_film` int(11) NOT NULL,
  `prix` decimal(10,0) NOT NULL,
  `stock` int(11) NOT NULL,
  PRIMARY KEY (`id_format_film`),
  KEY `fk_film_f` (`fk_film`),
  KEY `fk_format_f` (`fk_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `format`
--

DROP TABLE IF EXISTS `format`;
CREATE TABLE IF NOT EXISTS `format` (
  `id_format` int(11) NOT NULL AUTO_INCREMENT,
  `format` varchar(50) NOT NULL,
  PRIMARY KEY (`id_format`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `genre`
--

DROP TABLE IF EXISTS `genre`;
CREATE TABLE IF NOT EXISTS `genre` (
  `id_genre` int(11) NOT NULL AUTO_INCREMENT,
  `nom_genre` varchar(30) NOT NULL,
  PRIMARY KEY (`id_genre`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `genre`
--

INSERT INTO `genre` (`id_genre`, `nom_genre`) VALUES
(1, 'comedy'),
(2, 'drame'),
(3, 'action');

-- --------------------------------------------------------

--
-- Structure de la table `ligne_commande`
--

DROP TABLE IF EXISTS `ligne_commande`;
CREATE TABLE IF NOT EXISTS `ligne_commande` (
  `id_commande` int(11) NOT NULL AUTO_INCREMENT,
  `fk_panier` int(11) NOT NULL,
  `fk_film_format` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  PRIMARY KEY (`id_commande`),
  KEY `fk_panier` (`fk_panier`),
  KEY `fk_film_format` (`fk_film_format`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `panier`
--

DROP TABLE IF EXISTS `panier`;
CREATE TABLE IF NOT EXISTS `panier` (
  `id_panier` int(11) NOT NULL AUTO_INCREMENT,
  `fk_client` int(11) NOT NULL,
  `date` date NOT NULL,
  `statut` tinyint(1) NOT NULL,
  PRIMARY KEY (`id_panier`),
  KEY `fk_client` (`fk_client`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `personne`
--

DROP TABLE IF EXISTS `personne`;
CREATE TABLE IF NOT EXISTS `personne` (
  `id_personne` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(200) NOT NULL,
  `prenom` varchar(150) NOT NULL,
  PRIMARY KEY (`id_personne`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `personne`
--

INSERT INTO `personne` (`id_personne`, `nom`, `prenom`) VALUES
(1, 'simssss', 'voilalala'),
(2, 'Sahouli', 'Bilel');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `film`
--
ALTER TABLE `film`
  ADD CONSTRAINT `genre` FOREIGN KEY (`fk_genre`) REFERENCES `genre` (`id_genre`),
  ADD CONSTRAINT `realisateur` FOREIGN KEY (`fk_realisateur`) REFERENCES `personne` (`id_personne`);

--
-- Contraintes pour la table `film_acteur`
--
ALTER TABLE `film_acteur`
  ADD CONSTRAINT `fk_acteur` FOREIGN KEY (`fk_acteur`) REFERENCES `personne` (`id_personne`),
  ADD CONSTRAINT `fk_film` FOREIGN KEY (`fk_film`) REFERENCES `film` (`id_film`);

--
-- Contraintes pour la table `film_format`
--
ALTER TABLE `film_format`
  ADD CONSTRAINT `fk_film_f` FOREIGN KEY (`fk_film`) REFERENCES `film` (`id_film`),
  ADD CONSTRAINT `fk_format_f` FOREIGN KEY (`fk_format`) REFERENCES `format` (`id_format`);

--
-- Contraintes pour la table `ligne_commande`
--
ALTER TABLE `ligne_commande`
  ADD CONSTRAINT `fk_film_format` FOREIGN KEY (`fk_film_format`) REFERENCES `film_format` (`id_format_film`),
  ADD CONSTRAINT `fk_panier` FOREIGN KEY (`fk_panier`) REFERENCES `panier` (`id_panier`);

--
-- Contraintes pour la table `panier`
--
ALTER TABLE `panier`
  ADD CONSTRAINT `fk_client` FOREIGN KEY (`fk_client`) REFERENCES `client` (`id_client`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
