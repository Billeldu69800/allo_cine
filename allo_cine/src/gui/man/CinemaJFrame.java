package gui.man;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;

import com.toedter.calendar.JDateChooser;

import bs.film.model.Film;
import bs.film.model.Genre;
import bs.film.model.Personne;
import bs.jdbc.dao.FilmDao;
import bs.jdbc.dao.GenreDao;
import bs.jdbc.dao.PersonneDao;

public class CinemaJFrame extends JFrame {

	Genre genre;
	Personne personne;
	PersonneDao pDao = new PersonneDao();
	FilmDao fDao = new FilmDao();
	GenreDao gDao = new GenreDao();
	private JButton btnDelete;
	private static JComboBox cmbGenre;
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtRechercher;
	static JTable table;
	private static JTextField JtxtTitre;
	private static JTextField JtxtResume;
	private JTextField JtxtRealisateur;
	private JTextField Jtxtgenre;
	private JTextField JtxtPrenom;
	private static JComboBox cmbPerso;
	ActionCinemaWin acw = new ActionCinemaWin();
	private static JDateChooser tfDateSortie;
	private JButton btnModifier;
	JButton BtnAjout;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CinemaJFrame frame = new CinemaJFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public CinemaJFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1000, 833);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(51, 51, 51));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		init_ui();

	}

	private void init_ui(){
		JLabel LabelFilms = new JLabel("NETFLIX");
		LabelFilms.setBackground(new Color(51, 51, 51));
		LabelFilms.setForeground(new Color(139, 0, 0));
		LabelFilms.setFont(new Font("Tahoma", Font.BOLD, 20));
		LabelFilms.setHorizontalAlignment(SwingConstants.CENTER);
		LabelFilms.setBounds(107, 62, 134, 50);
		contentPane.add(LabelFilms);

		txtRechercher = new JTextField();
		txtRechercher.setHorizontalAlignment(SwingConstants.CENTER);
		txtRechercher.setFont(new Font("Tahoma", Font.BOLD, 12));
		txtRechercher.setBounds(660, 81, 134, 20);
		contentPane.add(txtRechercher);
		txtRechercher.setColumns(10);

		init_table();
	}

	private void init_table() {

		// Creation table (JTable) avec Model class
		// !!!Import� son package dao et le model dans le projet

		table = new JTable(new FilmTableModel());

		// R�alisation dun tableau qui peut scroller
		JScrollPane scp = new JScrollPane(table);
		List<Film>data;
		scp.setBounds(12, 121, 990, 235);
		contentPane.add(scp);

		/////////////////////////////COMBO BOX/////////////////////////////////////////////////
		PersonneDao pdao = new PersonneDao();

		List<Personne> persos = pdao.list();
		String[] realisateur_noms = new String[persos.size()];
		int i = 0;
		for (Personne v : persos) {
			realisateur_noms[i++] = v.getNom();
		}
		cmbPerso = new JComboBox<Object>(realisateur_noms);
		cmbPerso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});

		GenreDao genredao = new GenreDao();
		List<Genre> genre = genredao.list();
		String[] genre_noms = new String[genre.size()];
		int j = 0;
		for (Genre g : genre) {
			genre_noms[j++] = g.getNom_genre();
		}
		cmbGenre = new JComboBox<Object>(genre_noms);
		cmbGenre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});

		cmbGenre.setBounds(234, 475, 99, 20);
		contentPane.add(cmbGenre);

		cmbPerso.setBounds(107, 475, 99, 20);
		contentPane.add(cmbPerso);

		//////////////////////////////////////////MES JTEXT FIELD //////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////////////////////////////////

		JtxtTitre = new JTextField();
		JtxtTitre.setBounds(107, 420, 226, 20);
		contentPane.add(JtxtTitre);
		JtxtTitre.setColumns(10);

		JtxtResume = new JTextField();
		JtxtResume.setColumns(10);
		JtxtResume.setBounds(107, 557, 226, 72);
		contentPane.add(JtxtResume);

		JtxtRealisateur = new JTextField();
		JtxtRealisateur.setColumns(10);
		JtxtRealisateur.setBounds(470, 436, 86, 20);
		contentPane.add(JtxtRealisateur);

		JtxtPrenom = new JTextField();
		JtxtPrenom.setColumns(10);
		JtxtPrenom.setBounds(470, 525, 86, 20);
		contentPane.add(JtxtPrenom);

		////////////////////////////////////////////MES BOUTONS ADD MODIFIER UPDATE ....//////////////////////////////////////////////////////////////////
		btnModifier = new JButton("modified");
		btnModifier.setBounds(203, 657, 89, 23);
		contentPane.add(btnModifier);

		BtnAjout = new JButton("Ajouter");
		BtnAjout.setBounds(104, 657, 89, 23);
		contentPane.add(BtnAjout);

		 btnDelete = new JButton("delete");
		
		btnDelete.setBounds(302, 657, 89, 23);
		contentPane.add(btnDelete);



		JLabel lblFilms = new JLabel("Films");
		lblFilms.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblFilms.setForeground(Color.WHITE);
		lblFilms.setBounds(107, 367, 46, 14);
		contentPane.add(lblFilms);

		JLabel lblRalisateur = new JLabel("R\u00E9alisateur");
		lblRalisateur.setForeground(Color.WHITE);
		lblRalisateur.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblRalisateur.setBounds(470, 367, 113, 14);
		contentPane.add(lblRalisateur);

		JLabel lblTitre = new JLabel("TITRE");
		lblTitre.setForeground(Color.WHITE);
		lblTitre.setBounds(107, 407, 46, 14);
		contentPane.add(lblTitre);

		JLabel lblPrenom = new JLabel("Prenom ");
		lblPrenom.setForeground(Color.WHITE);
		lblPrenom.setBounds(470, 510, 46, 14);
		contentPane.add(lblPrenom);

		JLabel lblNom = new JLabel("NOM");
		lblNom.setForeground(Color.WHITE);
		lblNom.setBounds(470, 425, 46, 14);
		contentPane.add(lblNom);

		JLabel lblActeur = new JLabel("Acteur ");
		lblActeur.setForeground(Color.WHITE);
		lblActeur.setBounds(107, 459, 46, 14);
		contentPane.add(lblActeur);

		JLabel lblResumer = new JLabel("Resumer");
		lblResumer.setForeground(Color.WHITE);
		lblResumer.setBounds(107, 544, 46, 14);
		contentPane.add(lblResumer);


		JLabel lblGebre = new JLabel("Genre");
		lblGebre.setForeground(Color.WHITE);
		lblGebre.setBounds(234, 459, 46, 14);
		contentPane.add(lblGebre);
		
		tfDateSortie = createCalendarChooser();
		tfDateSortie.setBounds(104, 506, 156, 20);
		contentPane.add(tfDateSortie);

		formatColumns();
		handleclickTable();
		actionButton();

	}
	private void actionButton() {
		BtnAjout.addActionListener(acw) ;

	}

	private void handleclickTable() {
		table.addMouseListener(new MouseListener() {

			@Override
			public void mouseReleased(MouseEvent e) {
				int index = table.getSelectedRow();
				System.out.println(index);
				Film item = FilmTableModel.data.get(index);
				System.out.println("----->"+item);
				JtxtTitre.setText(item.getTitre());
				JtxtResume.setText(item.getResume() );
				JtxtRealisateur.setText(item.getFk_realisateur().getNom() );
				JtxtPrenom.setText(item.getFk_realisateur().getPrenom() );
				
			
				 btnDelete.addActionListener(new ActionListener() {
					 	public void actionPerformed(ActionEvent e) {
					 		
					 		fDao.delete(item);
							JOptionPane.showMessageDialog(null, "Votre film � bien �t� supprimer", "Information", JOptionPane.INFORMATION_MESSAGE);
							reloadTable();
							formatColumns();
					 	}
					 });
				
				 
				 
				 btnModifier.addActionListener(new ActionListener() {
					 	public void actionPerformed(ActionEvent e) {
					 		int row = table.getSelectedRow();
							System.out.println(table.getModel().getValueAt(row, 0).toString());
							int idFilm = Integer.parseInt(table.getModel().getValueAt(row, 0).toString());
					 		
							String titre = JtxtTitre.getText();
							String resume = JtxtResume.getText();
							String nomRealisateur = getCmbPerso().getSelectedItem().toString();
							String stGenre = getCmbGenre().getSelectedItem().toString();
							genre=gDao.find(stGenre);
							personne = pDao.find(nomRealisateur);
							LocalDate date_sortie = getTfDateSortie().getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
							Film film =fDao.find(idFilm);
							
							film.setTitre(titre);
							film.setDate_sortie(date_sortie);
							film.setResume(resume);
							film.setFk_realisateur(personne);
							film.setFk_genre(genre);
							
							System.out.println(film);
							fDao.Uptdate(film);
							 reloadTable();
					 	}
					 });
				 
				 
				 
				 
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub

			}
		});
	}

	private void formatColumns() {
		// personaliser pr�sentaion des colonnes
		DefaultTableCellRenderer centerRedRenderer = new DefaultTableCellRenderer();
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		DefaultTableCellRenderer leftRenderer = new DefaultTableCellRenderer();

		leftRenderer.setHorizontalAlignment(JLabel.LEFT);
		leftRenderer.setForeground(Color.PINK);
		table.setDefaultRenderer(LocalDate.class, leftRenderer);

		rightRenderer.setHorizontalAlignment(JLabel.RIGHT);
		rightRenderer.setForeground(Color.BLUE);
		table.setDefaultRenderer(String.class, rightRenderer);

		// Appliquer a toute les colonnes
		centerRedRenderer.setHorizontalAlignment(JLabel.CENTER);
		centerRedRenderer.setForeground(Color.RED);

		// Appliquer � une colonne
		table.getColumnModel().getColumn(0).setCellRenderer(centerRedRenderer);
		table.getColumnModel().getColumn(0).setPreferredWidth(10);
	}


	public JTable getTable() {
		return table;
	}

	public void setTable(JTable table) {
		this.table = table;
	}

	public static JTextField getJtxtTitre() {
		return JtxtTitre;
	}

	public void setJtxtTitre(JTextField jtxtTitre) {
		JtxtTitre = jtxtTitre;
	}

	public static JTextField getJtxtResume() {
		return JtxtResume;
	}

	public void setJtxtResume(JTextField jtxtResume) {
		JtxtResume = jtxtResume;
	}

	public JTextField getJtxtRealisateur() {
		return JtxtRealisateur;
	}

	public void setJtxtRealisateur(JTextField jtxtRealisateur) {
		JtxtRealisateur = jtxtRealisateur;
	}

	public JTextField getJtxtgenre() {
		return Jtxtgenre;
	}

	public void setJtxtgenre(JTextField jtxtgenre) {
		Jtxtgenre = jtxtgenre;
	}

	public JTextField getJtxtPrenom() {
		return JtxtPrenom;
	}

	public void setJtxtPrenom(JTextField jtxtPrenom) {
		JtxtPrenom = jtxtPrenom;
	}

	public static JComboBox getCmbPerso() {
		return cmbPerso;
	}

	public void setCmbPerso(JComboBox cmbPerso) {
		this.cmbPerso = cmbPerso;
	}

	public static JComboBox getCmbGenre() {
		return cmbGenre;
	}

	public void setCmbGenre(JComboBox cmbGenre) {
		this.cmbGenre = cmbGenre;
	}
	
	
	
	
	
	public static JDateChooser getTfDateSortie() {
		return tfDateSortie;
	}

	public void setTfDateSortie(JDateChooser tfDateSortie) {
		this.tfDateSortie = tfDateSortie;
	}

	private JDateChooser createCalendarChooser() { 
		LocalDate currentDate = LocalDate.now(); //.parse(currentvalue.toString(), DateTimeFormatter.ofPattern("dd/MM/yyyy"));                     
		Date date = Date.from(currentDate.atStartOfDay(ZoneId.systemDefault()).toInstant());         
		JDateChooser jdchooser = new JDateChooser();         
		jdchooser.setDate(date);         
		return jdchooser;     
		}
	

	private void reloadTable() {
        CinemaJFrame.table.setModel(new FilmTableModel());
        CinemaJFrame.table.repaint();
    }
	
	
	
}








class FilmTableModel extends AbstractTableModel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static List<Film>data;

	String[] columns = {"#","titre","date de sortie", "r�sum�","cover","realisateur","genre"};
	public List<Integer> updated = new ArrayList<Integer>();
	public List<Integer> deleted = new ArrayList<Integer>();

	public FilmTableModel(){
		FilmDao daoFilm = new FilmDao();
		data = daoFilm.list();
		//System.out.println(data);
	}
	public void getvaluat(int row) {
		// TODO Auto-generated method stub

	}
	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return columns.length;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return data.size();
	}

	public String getColumnName(int column){
		return columns[column];
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		Film item = data.get(rowIndex);
		switch (columnIndex) {
		case 0:
			return item.getId_film();
		case 1:
			return item.getTitre();
		case 2:
			return item.getDate_sortie();
		case 3:
			return item.getResume();
		case 4:
			return item.getCover();
		case 5:
			return item.getFk_realisateur().getNom();
		case 6:
			return item.getFk_genre().getNom_genre();

		default:
			return new Boolean(deleted.contains(rowIndex));
		}
	}


	public Class<?> getColumnClass(int columnIndex){
		switch(columnIndex){

		case 0:
			return Integer.class;

		case 1:	
			return String.class;

		case 3:
			return String.class;

		case 4:
			return String.class;

		case 5:
			return String.class;

		case 6:
			return String.class;
		case 2: 
			return LocalDate.class;
		default:
			return Boolean.class;
		}
	}
	public boolean isCellEditable(int rowIndex, int columnIndex){
		return(columnIndex > 0);
	}
	public void setValueAt(Object value, int rowIndex,int columnIndex){
		Film item = data.get(rowIndex);
		Boolean changed = false;
		System.out.println("colonne index : " + columnIndex + "=> " + value);

		if (columnIndex < columns.length - 1) {
			changed = updateItemField(item, rowIndex, columnIndex,value);
			if(changed){
				fireTableCellUpdated(rowIndex, columnIndex);
			}
		}else {
			boolean selected = deleted.contains(rowIndex);
			if(selected){
				deleted.remove((Object) rowIndex);
			}else {
				deleted.add(rowIndex);
			}
			if ((boolean) value){
				fireTableRowsDeleted(rowIndex, columnIndex);
			}
		}
	}



	private Boolean updateItemField(Film item, int rowIndex,
			int columnIndex, Object value) {
		boolean changed = false;
		LocalDate date_sortie = null;
		switch (columnIndex) {
		case 1:
			System.out.println(item.getTitre() + " ==>" + value.toString());
			changed = !item.getTitre().equalsIgnoreCase(
					value.toString().trim());
			if (changed)
				item.setTitre(value.toString());
			System.out.println(item);
			break;
		case 2:
			try {
				changed = !item.getDate_sortie().equals(date_sortie);
				if (changed)
					item.setDate_sortie(date_sortie);
			} catch (Exception e) {
				System.out.println(value.toString().trim()
						+ "  : format date non valide!");
			}
			break;
		case 3:


			break;
		case 4:
			// item.setVille(...);
			break;
		}
		// updated est une liste qui contient le num�ro des rang�es qui ont
		// �t� aff�ct�es
		if (changed && !updated.contains(rowIndex))
			updated.add(rowIndex);
		return changed;
	}








	public void table_FilmClicked(java.awt.event.MouseEvent evt, JTable table){
		try {
			int row = table.getSelectedRow();
			table.getModel().getValueAt(row, 0).toString();
		} catch (Exception e) {
			// TODO: handle exception

			JOptionPane.showMessageDialog(null, e);
		}
	}








}

