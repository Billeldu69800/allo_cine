package gui.man;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.ZoneId;

import javax.swing.JButton;

import bs.film.model.Film;
import bs.film.model.Genre;
import bs.film.model.Personne;
import bs.jdbc.dao.FilmDao;
import bs.jdbc.dao.GenreDao;
import bs.jdbc.dao.PersonneDao;

public class ActionCinemaWin implements ActionListener {
	Genre genre;
	Personne personne;
	PersonneDao pDao = new PersonneDao();
	FilmDao fDao = new FilmDao();
	GenreDao gDao = new GenreDao();
	
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton btn = (JButton) e.getSource();



		switch (btn.getActionCommand()) {
		
			
		case"Ajouter":
			genre= new Genre();
			String titre = CinemaJFrame.getJtxtTitre().getText();
			String resume = CinemaJFrame.getJtxtResume().getText();
			String nomRealisateur = CinemaJFrame.getCmbPerso().getSelectedItem().toString();
			String stGenre = CinemaJFrame.getCmbGenre().getSelectedItem().toString();
			genre=gDao.find(stGenre);
			personne = pDao.find(nomRealisateur);
			LocalDate date_sortie = CinemaJFrame.getTfDateSortie().getDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

			Film film = new Film(titre, date_sortie, resume,personne,genre);
			
			System.out.println(film);
			fDao.add(film);

			break;
	
			
	
			
			
			
		default:
			break;
		}
	}

	private void reloadTable() {
        CinemaJFrame.table.setModel(new FilmTableModel());
        CinemaJFrame.table.repaint();
    }
	
	
	
	
	
	
}
