package bs.jdbc.dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import bs.film.model.Genre;



public class GenreDao implements IDao<Genre> {

	@Override
	public List<Genre> list() {
		List <Genre> genres = new ArrayList<Genre>();		
		Genre genre = null;

		try {
			// executer requete : (exp)liste users (table)
			Statement stmt = connection.createStatement();
			//lire donnee (SELECT)
			String sql = "SELECT * FROM genre ORDER BY id_genre";
			ResultSet rs = stmt.executeQuery(sql);


			while(rs.next()){
				// enregistrement de la table a l'objet de type users
				genre = new Genre(rs.getInt("id_genre"),rs.getString("nom_genre"));
				genres.add(genre);
			}
			rs.close();
			stmt.close();
		} catch (Exception e) {
			// TODO: handle exception
		}

		return genres;
	}

	@Override
	public Genre find(int id_genre) {
		Genre genre = null;
		try {
			Statement stmt = connection.createStatement();
			String sql = "SELECT * FROM genre WHERE id_genre="+ id_genre;
			ResultSet rs = stmt.executeQuery(sql);
			if(rs.next()){
				genre = new Genre(rs.getInt("id_genre"),rs.getString("nom_genre"));
			}
			rs.close();
			stmt.close();
			
		} catch (Exception e) {
			System.out.println("Error : "+e);
		}
		return genre;
	}

	@Override
	public boolean add(Genre item) {
		String sql = "INSERT INTO genre (nom_genre) VALUES ('"+item.getNom_genre()+"')";
		int resultat = 0;
		
		System.out.println(sql);
		
		try {
			Statement stmt = connection.createStatement();
			resultat = stmt.executeUpdate(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultat>0;
		
	}

	@Override
	public boolean delete(Genre item) {
		String sql="DELETE FROM genre WHERE id="+item.getId_genre();
		int resultat=0;		
		try {
			Statement stmt = connection.createStatement();	
			resultat = stmt.executeUpdate(sql);			
		} catch (Exception e) {
			System.out.println("Error : "+e);
		}
		return (resultat>0);
	}

	@Override
	public boolean Uptdate(Genre item) {
		String sql="UPDATE genre SET nom_genre='"+item.getNom_genre();
		int resultat=0;		
		try {
			Statement stmt = connection.createStatement();	
			resultat = stmt.executeUpdate(sql);			
		} catch (Exception e) {
			System.out.println("Error : "+e);
		}
		return (resultat>0);
	}

	

	public Genre find(String nom) {
		Genre genre = null;
		try {
			Statement stmt = connection.createStatement();
			String sql = "SELECT * FROM genre WHERE nom_genre='" +nom+"'";
			ResultSet rs = stmt.executeQuery(sql);
			if(rs.next()){
				genre = new Genre(rs.getInt("id_genre"),rs.getString("nom_genre"));
			}
			rs.close();
			stmt.close();
			
		} catch (Exception e) {
			//System.out.println("Error : "+e);
			e.printStackTrace();
		}
		return genre;
	}


}
