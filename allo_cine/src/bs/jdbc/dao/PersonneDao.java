package bs.jdbc.dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import bs.film.model.Personne;



public class PersonneDao implements IDao<Personne> {

	@Override
	public List<Personne> list() {
		List <Personne> personnes = new ArrayList<Personne>();		
		Personne personne = null;

	try {
		// executer requete : (exp)liste users (table)
		Statement stmt = connection.createStatement();
		//lire donnee (SELECT)
		String sql = "SELECT * FROM personne ORDER BY id_personne";
		ResultSet rs = stmt.executeQuery(sql);
		

	
		while(rs.next()){
			// enregistrement de la table a l'objet de type users
			personne = new Personne(rs.getInt("id_personne"),rs.getString("nom"),rs.getString("prenom"));
			personnes.add(personne);
		}
		rs.close();
		stmt.close();
	} catch (Exception e) {
		// TODO: handle exception
	}

		return personnes;
	}

	@Override
	public Personne find(int id_personne) {
		Personne personne = null;
		try {
			Statement stmt = connection.createStatement();
			String sql = "SELECT * FROM personne WHERE id_personne="+ id_personne;
			ResultSet rs = stmt.executeQuery(sql);
			if(rs.next()){
				personne = new Personne(rs.getInt("id_personne"),rs.getString("nom"),rs.getString("prenom"));
			}
			rs.close();
			stmt.close();
			
		} catch (Exception e) {
			System.out.println("Error : "+e);
		}
		return personne;
	}

	@Override
	public boolean add(Personne item) {
		String sql = "INSERT INTO personne (nom,prenom) VALUES ('"+item.getNom()+"', '"+item.getPrenom()+"')";
		int resultat = 0;
		
		System.out.println(sql);
		
		try {
			Statement stmt = connection.createStatement();
			resultat = stmt.executeUpdate(sql);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return resultat>0;
	}

	@Override
	public boolean delete(Personne item) {
		String sql="DELETE FROM users WHERE id="+item.getId_personne();
		int resultat=0;		
		try {
			Statement stmt = connection.createStatement();	
			resultat = stmt.executeUpdate(sql);			
		} catch (Exception e) {
			System.out.println("Error : "+e);
		}
		return (resultat>0);
	}

	@Override
	public boolean Uptdate(Personne item) {
		String sql="UPDATE personne SET nom='"+item.getNom()+"', prenom='"+item.getPrenom();
		int resultat=0;		
		try {
			Statement stmt = connection.createStatement();	
			resultat = stmt.executeUpdate(sql);			
		} catch (Exception e) {
			System.out.println("Error : "+e);
		}
		return (resultat>0);
	}
	
	
	public Personne find(String nom){
		Personne personne = null;
		try {
			Statement stmt = connection.createStatement();
			String sql = "SELECT * FROM personne WHERE nom='"+nom+"'";
			ResultSet rs = stmt.executeQuery(sql);
			if(rs.next()){
				personne = new Personne(rs.getInt("id_personne"),rs.getString("nom"),rs.getString("prenom"));
			}
			rs.close();
			stmt.close();
			
		} catch (Exception e) {
			//System.out.println("Error : "+e);
			e.printStackTrace();

		}
		return personne;
	}
	
}
