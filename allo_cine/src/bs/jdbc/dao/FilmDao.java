
package bs.jdbc.dao;

import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;









import bs.film.model.Film;
import bs.film.model.Genre;
import bs.film.model.Personne;

public class FilmDao implements IDao<Film>{
	PersonneDao pDao = new PersonneDao();
	GenreDao gDao = new GenreDao();
	Film film;
    Genre genre;
    Personne personne;
	@Override
	public List<Film> list() {
		List <Film> films = new ArrayList<Film>();		
		Film film = null;

		try {
			// executer requete : (exp)liste users (table)
			Statement stmt = connection.createStatement();
			//lire donnee (SELECT)
			String sql = "SELECT * FROM film ORDER BY id_film";
			ResultSet rs = stmt.executeQuery(sql);

			GenreDao GenreDao = new GenreDao();
			PersonneDao PersonneDao = new PersonneDao();

			while(rs.next()){

				film = new Film(rs.getInt("id_film"),rs.getString("titre"), LocalDate.parse(rs.getString("date_sortie")),rs.getString("resume"),
						rs.getString("cover"),PersonneDao.find(rs.getInt("fk_realisateur")),GenreDao.find(rs.getInt("fk_genre")));
				films.add(film);
			}
			rs.close();
			stmt.close();
		} catch (Exception e) {
			// TODO: handle exception
		}

		return films;
	}

	@Override
	public Film find(int id_film) {
		Film film =null;
		GenreDao GenreDao = new GenreDao();
		PersonneDao PersonneDao = new PersonneDao();

		try {
			Statement stmt = connection.createStatement();
			String sql = "SELECT * FROM film WHERE id_film= "+ id_film ;
			ResultSet rs = stmt.executeQuery(sql);
			if(rs.next()){

				film = new Film(rs.getInt("id_film"),rs.getString("titre"), LocalDate.parse(rs.getString("date_sortie")),rs.getString("resume"),
						rs.getString("cover"),PersonneDao.find(rs.getInt("fk_realisateur")),GenreDao.find(rs.getInt("fk_genre")));
			}
			rs.close();
			stmt.close();

		} catch (Exception e) {
			System.out.println("Error : " + e);		
		}
		return film;
	}

	@Override
	public boolean add(Film item) {
		int resultat = 0;

		if (this.find(item.getId_film())==null){
			
			if (gDao.find(item.getFk_genre().getNom_genre())==null)
				gDao.add(item.getFk_genre());

			item.setFk_genre(gDao.find(item.getFk_genre().getNom_genre()));
			System.out.println("=====>"+item.getFk_genre());


			if (pDao.find(item.getFk_realisateur().getNom())== null)
				pDao.add(item.getFk_realisateur());

			item.setFk_realisateur(pDao.find(item.getFk_realisateur().getNom()));
			System.out.println("============>"+item.getFk_realisateur());


			String sql="INSERT INTO `film` (`titre`, `date_sortie`, `resume`, `cover`, `fk_realisateur`, `fk_genre`) "
					+ "VALUES ('"+item.getTitre()+"', '"+item.getDate_sortie()+"', '"+item.getResume()+"', '"+item.getCover()+"', '"+item.getFk_realisateur().getId_personne()+"', '"+item.getFk_genre().getId_genre()+"')";
			try {
				Statement stmt = connection.createStatement();	
				resultat = stmt.executeUpdate(sql);			
				System.out.println("Film d�ja a bien �t� ajout� dans la bdd");

			} catch (Exception e) {
				//System.out.println("Error : "+e);
				e.printStackTrace();
			}

		}else{
			System.out.println("Film d�ja existant dans la bdd");
		}



		return (resultat >0);

	}

	@Override
	public boolean delete(Film item) {
		String sql="DELETE FROM film WHERE id_film="+item.getId_film()+"";
		int resultat=0;		
		try {
			Statement stmt = connection.createStatement();	
			resultat = stmt.executeUpdate(sql);			
		} catch (Exception e) {
			
			System.out.println("Error : "+e);
		}
		return (resultat>0);
	}

	@Override
	public boolean Uptdate(Film item) {
		String sql="UPDATE film SET titre='"+item.getTitre()+"', date_sortie='"+item.getDate_sortie()+"',"
				+ " resume='"+item.getResume()+"' "
				+ ", cover="+item.getCover()+" WHERE id_film='"+item.getId_film()+"'";
		int resultat=0;		
		try {
			Statement stmt = connection.createStatement();	
			resultat = stmt.executeUpdate(sql);			
		} catch (Exception e) {
			e.printStackTrace();
			//System.out.println("Error : "+e);
		}
		return (resultat>0);
	}

	public Film find(String nom) {
        try {
            //1- L'objet Statement permet de faire le dialoge entre JAVA et SQL
            Statement stmt = connection.createStatement();
            boolean empty = true;

            String sql2 = "SELECT film.id_film AS `ID Film`,"
                    + " film.titre AS `Titre`,"
                    + " film.date_sortie AS `Date de sortie`,"
                    + " film.resume AS `Resume`,"
                    + " film.cover AS `Jacquette`,"
                    + " genre.nom_genre AS `Genre`,"
                    + " personne.prenom AS `R�alisateur` FROM film" + 
                    " INNER JOIN personne ON personne.id_personne = film.fk_realisateur" + 
                    " INNER JOIN genre on genre.id_genre = film.fk_genre" + 
                    " WHERE film.titre='" +nom + "' GROUP BY id_film";


            //System.out.println(sql2);
            //2- Execution 1ere requete SQL 

            // REQUETE POUR SELECT LA LISTE DES FILM 
            ResultSet rs = stmt.executeQuery(sql2);

            while (rs.next()) {

                genre = gDao.find(rs.getString("Genre"));
                personne =pDao.find(rs.getString("R�alisateur"));
                film = new Film(rs.getInt(1),
                        rs.getString("titre"),
                        LocalDate.parse(rs.getString(3)),
                        rs.getString("resume"),
                        rs.getString("Jacquette"),
                        genre, 
                        personne);
                empty = false;
            }

            if (empty) {
                System.out.println("Aucune donn�es trouv�es");
            }
            rs.close();
            stmt.close();
        }catch (Exception e) {
        	e.printStackTrace();
           // System.out.println("Erreur: " +e);
            //e.printStackTrace();
        }
        return film;
    }

}
