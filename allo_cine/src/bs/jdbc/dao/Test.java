package bs.jdbc.dao;
import java.time.LocalDate;
import java.util.List;

import bs.film.model.Film;
import bs.film.model.Genre;
import bs.film.model.Personne;


public class Test {

	public static void main(String[] args) {
		FilmDao daoFilm = new FilmDao();
		List<Film> films = daoFilm.list();

		for (Film film : films) {
			System.out.println(film);
		}

		GenreDao daoGenre = new GenreDao();
		List<Genre> genres= daoGenre.list();		
		for (Genre genre : genres){
			System.out.println(genre);
		}
		PersonneDao daoPersonne = new PersonneDao();
		List<Personne> personnes = daoPersonne.list();
		for (Personne personne : personnes) {
			System.out.println(personne);
		}
		Genre g = daoGenre.find(1);
		Personne p = daoPersonne.find(1);
		
	//	boolean ok = daoFilm.add(new Film("Naruto",LocalDate.of(1994, 04, 16),"naruto ultimate","fxgdgdrg",p,g));
	
//		if (ok){
//			System.out.println("User ajouter");
//		}
	}



}
