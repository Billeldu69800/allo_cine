package bs.film.model;

public class ligne_commande {

	private int id_commande;
	private int quantite;
	
	private Panier fk_panier;
	private Format fk_film_format;
	public int getId_commande() {
		return id_commande;
	}
	public void setId_commande(int id_commande) {
		this.id_commande = id_commande;
	}
	public int getQuantite() {
		return quantite;
	}
	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
	public Panier getFk_panier() {
		return fk_panier;
	}
	public void setFk_panier(Panier fk_panier) {
		this.fk_panier = fk_panier;
	}
	public Format getFk_film_format() {
		return fk_film_format;
	}
	public void setFk_film_format(Format fk_film_format) {
		this.fk_film_format = fk_film_format;
	}

}
