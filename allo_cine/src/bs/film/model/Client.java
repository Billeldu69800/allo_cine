package bs.film.model;

public class Client {
	
	private int id_client;
	private String pseudo;
	private String mail;
	private String mot_de_passe;
////////////////////////////////////////////////////////////////////////////////////////////////////
	public Client() {

	}

	public Client(int id_client, String pseudo, String mail, String mot_de_passe) {
		this.id_client = id_client;
		this.pseudo = pseudo;
		this.mail = mail;
		this.mot_de_passe = mot_de_passe;
	}
	
	public Client(String pseudo, String mail, String mot_de_passe) {
		this.pseudo = pseudo;
		this.mail = mail;
		this.mot_de_passe = mot_de_passe;
	}
////////////////////////////////////////////////////////////////////////////////////////////////////
	public int getId_client() {
		return id_client;
	}

	public void setId_client(int id_client) {
		this.id_client = id_client;
	}

	public String getPseudo() {
		return pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getMot_de_passe() {
		return mot_de_passe;
	}

	public void setMot_de_passe(String mot_de_passe) {
		this.mot_de_passe = mot_de_passe;
	}
////////////////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public String toString() {
		return "client [id_client=" + id_client + ", pseudo=" + pseudo
				+ ", mail=" + mail + ", mot_de_passe=" + mot_de_passe + "]";
	}
	
	
}
