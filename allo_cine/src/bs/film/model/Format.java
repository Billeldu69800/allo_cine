package bs.film.model;

public class Format {
	private int id_fomat;
	private String format;
////////////////////////////////////////////////////////////////////////////////////////////////////
	public Format() {
	}
	public Format(int id_fomat, String format) {
		this.id_fomat = id_fomat;
		this.format = format;
	}
	public Format(String format) {
		this.format = format;
	}
////////////////////////////////////////////////////////////////////////////////////////////////////
	public int getId_fomat() {
		return id_fomat;
	}
	public void setId_fomat(int id_fomat) {
		this.id_fomat = id_fomat;
	}
	public String getFormat() {
		return format;
	}
	public void setFormat(String format) {
		this.format = format;
	}
////////////////////////////////////////////////////////////////////////////////////////////////////
	public String toString() {
		return "Format [id_fomat=" + id_fomat + ", format=" + format + "]";
	}
	
}
