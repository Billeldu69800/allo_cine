package bs.film.model;

import java.util.Date;

public class Panier {

	private int id_panier;
	private Date date_panier;
	private boolean statut;
	private Client fk_client;
	public int getId_panier() {
		return id_panier;
	}
	public void setId_panier(int id_panier) {
		this.id_panier = id_panier;
	}
	public Date getDate_panier() {
		return date_panier;
	}
	public void setDate_panier(Date date_panier) {
		this.date_panier = date_panier;
	}
	public boolean isStatut() {
		return statut;
	}
	public void setStatut(boolean statut) {
		this.statut = statut;
	}
	public Client getFk_client() {
		return fk_client;
	}
	public void setFk_client(Client fk_client) {
		this.fk_client = fk_client;
	}
	
}
