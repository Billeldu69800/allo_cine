package bs.film.model;

import java.time.LocalDate;

public class Film {
	
	//VARIABLE////////////////////////////////////////////////////////////////////////////////////////////////////
	private int id_film;
	private String titre;
	private LocalDate date_sortie;
	private String resume;
	private String cover;
    
    //FOREIGN KEY////////////////////////////////////////////////////////////////////////////////////////////////////
	private Personne fk_realisateur;
	private Genre fk_genre;
	
	//CONSTRUCTEUR////////////////////////////////////////////////////////////////////////////////////////////////////
	public Film(int id_film, String titre, LocalDate date_sortie, String resume, String cover, Genre fk_genre, Personne fk_realisateur) {
		this.id_film = id_film;
		this.titre = titre;
		this.date_sortie = date_sortie;
		this.resume = resume;
		this.cover = cover;
		this.fk_genre = fk_genre;
		this.fk_realisateur = fk_realisateur;
	
	}

	public Film(int id_film, String titre, LocalDate date_sortie,
			String resume, String cover, Personne fk_realisateur, Genre fk_genre) {
		this.id_film = id_film;
		this.titre = titre;
		this.date_sortie = date_sortie;
		this.resume = resume;
		this.cover = cover;
		this.fk_realisateur = fk_realisateur;
		this.fk_genre = fk_genre;
	}




	public Film(String titre, String resume) {
		super();
		this.titre = titre;
		this.resume = resume;
	}

	public Film(String titre, LocalDate date_sortie, String resume,
			 Personne fk_realisateur, Genre fk_genre) {
		this.titre = titre;
		this.date_sortie = date_sortie;
		this.resume = resume;
		this.fk_realisateur = fk_realisateur;
		this.fk_genre = fk_genre;
	}
	


	public int getId_film() {
		return id_film;
	}



	public void setId_film(int id_film) {
		this.id_film = id_film;
	}



	public String getTitre() {
		return titre;
	}



	public void setTitre(String titre) {
		this.titre = titre;
	}



	public LocalDate getDate_sortie() {
		return date_sortie;
	}



	public void setDate_sortie(LocalDate date_sortie) {
		this.date_sortie = date_sortie;
	}



	public String getResume() {
		return resume;
	}



	public void setResume(String resume) {
		this.resume = resume;
	}



	public String getCover() {
		return cover;
	}



	public void setCover(String cover) {
		this.cover = cover;
	}



	public Personne getFk_realisateur() {
		return fk_realisateur;
	}



	public void setFk_realisateur(Personne fk_realisateur) {
		this.fk_realisateur = fk_realisateur;
	}



	public Genre getFk_genre() {
		return fk_genre;
	}



	public void setFk_genre(Genre fk_genre) {
		this.fk_genre = fk_genre;
	}

	@Override
	public String toString() {
		return "Film [id_film=" + id_film + ", titre=" + titre
				+ ", date_sortie=" + date_sortie + ", resume=" + resume
				+ ", cover=" + cover + ", fk_realisateur=" + fk_realisateur
				+ ", fk_genre=" + fk_genre + "]";
	}

}
