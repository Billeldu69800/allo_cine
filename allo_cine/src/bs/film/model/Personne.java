package bs.film.model;

public class Personne {
////////////////////////////////////////////////////////////////////////////////////////////////////
	private int id_personne;
	private String nom;
	private String prenom;
////////////////////////////////////////////////////////////////////////////////////////////////////
	public Personne(){
		
	}

	public Personne(int id_personne, String nom, String prenom) {
		this.id_personne = id_personne;
		this.nom = nom;
		this.prenom = prenom;
	}
	
	public Personne(String nom, String prenom) {
		this.nom = nom;
		this.prenom = prenom;
	}
////////////////////////////////////////////////////////////////////////////////////////////////////
	public int getId_personne() {
		return id_personne;
	}

	public void setId_personne(int id_personne) {
		this.id_personne = id_personne;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
////////////////////////////////////////////////////////////////////////////////////////////////////

	@Override
	public String toString() {
		return "Personne [id_personne=" + id_personne + ", nom=" + nom
				+ ", prenom=" + prenom + "]";
	}

	
}
